### Set GitLab CI/CD Variables
+ To set these variables open your GitLab repository, open Settings, click on *CI/CD and the scroll down and extend Variables
+ Make sure to set all your variables to mark as secret as masked to hide it from the pipeline output.
+ AWS_ACCESS_KEY_ID — IAM Access key ID;
+ AWS_SECRET_ACCESS_KEY — IAM Access key;
+ AWS_DEFAULT_REGION — Default region to be used by IAM user;

![image](https://user-images.githubusercontent.com/19683199/168238365-0c8f6296-15f0-47d3-8e80-55a646ce68b5.png)

### main.tf
+ Terraform code in this file

### .gitlab-ci.yml
This file holds all informations and steps used by the pipeline.

### Merge request pipeline
![image](https://user-images.githubusercontent.com/19683199/168238995-47291490-127b-430c-9d20-3fe16afe9f2a.png)

### Merge to main branch pipeline
![image](https://user-images.githubusercontent.com/19683199/168238114-b6a53fd8-55b3-41d0-8731-02c951703d75.png)
